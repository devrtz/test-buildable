#ifndef __MY_APP_H
#define __MY_APP_H

#include <gtk/gtk.h>


#define MY_TYPE_APP (my_app_get_type ())
G_DECLARE_FINAL_TYPE (MyApp, my_app, MY, APP, GtkApplication)


MyApp     *my_app_new         (void);


#endif /* __MY_APP_H */
