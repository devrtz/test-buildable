#include <gtk/gtk.h>

#include "my-window.h"
#include "my-app.h"

struct _MyApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE(MyApp, my_app, GTK_TYPE_APPLICATION);

static void
my_app_init (MyApp *app)
{
}

static void
my_app_activate (GApplication *app)
{
  MyWindow *window;

  window = my_window_new (MY_APP (app));
  gtk_window_present (GTK_WINDOW (window));
}

static void
my_app_class_init (MyAppClass *class)
{
  G_APPLICATION_CLASS (class)->activate = my_app_activate;
}

MyApp *
my_app_new (void)
{
  return g_object_new (MY_TYPE_APP,
                       "application-id", "org.gtk-builder.example",
                       "flags", G_APPLICATION_FLAGS_NONE,
                       NULL);
}
