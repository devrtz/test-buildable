#ifndef __MY_WIDGET_H__
#define __MY_WIDGET_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MY_TYPE_WIDGET (my_widget_get_type ())

G_DECLARE_FINAL_TYPE (MyWidget, my_widget, MY, WIDGET, GtkBox)

G_END_DECLS

#endif
