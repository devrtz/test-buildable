#ifndef __MY_WINDOW__
#define __MY_WINDOW__

#include "my-app.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MY_TYPE_WINDOW (my_window_get_type ())

G_DECLARE_FINAL_TYPE (MyWindow, my_window, MY, WINDOW, GtkApplicationWindow)

G_END_DECLS

MyWindow * my_window_new (MyApp *);

#endif
