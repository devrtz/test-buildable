#include "my-window.h"
#include "my-widget.h"

struct _MyWindow
{
  GtkApplicationWindow parent;

  GtkWidget           *my_widget;
};

G_DEFINE_TYPE (MyWindow, my_window, GTK_TYPE_APPLICATION_WINDOW)

MyWindow *
my_window_new (MyApp *app)
{
  return g_object_new (MY_TYPE_WINDOW,
                       "application", GTK_APPLICATION (app),
                       NULL);
}

static void
my_window_class_init (MyWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (MY_TYPE_WIDGET);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gtk-builder/example/my-window.ui");

  gtk_widget_class_bind_template_child (widget_class, MyWindow, my_widget);
}

static void
my_window_init (MyWindow *self)
{
  GApplication *app;
  const gchar *quit_accels[2] = { "<Ctrl>q", NULL};

  gtk_widget_init_template (GTK_WIDGET (self));

  app = g_application_get_default ();

  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.quit", quit_accels);
}
