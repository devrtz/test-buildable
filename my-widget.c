#include "my-widget.h"

struct _MyWidget
{
  GtkBox              parent;

  /* Child widgets */
  GtkWidget          *header;
  GtkWidget          *grid;

  GtkWidget          *grid_cells[3][4]; /* unowned */
};

static void gtk_buildable_interface_init    (GtkBuildableIface *iface);

/* XXX child types in .ui file need to be deleted when not implementing buildable interface */
//G_DEFINE_TYPE (MyWidget, my_widget, GTK_TYPE_BOX) 

G_DEFINE_TYPE_WITH_CODE (MyWidget, my_widget, GTK_TYPE_BOX,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE,
                                                gtk_buildable_interface_init));



static void
setup_header_widget (MyWidget  *self,
                     GtkWidget *widget)
{
  self->header = widget;
  gtk_widget_set_parent (widget, GTK_WIDGET (self));
}

static void
setup_cell_grid (MyWidget  *self,
                 GtkWidget *widget)
{
  guint row, col;
  g_autofree gchar *text = NULL;

  self->grid = widget;
  gtk_widget_set_parent (widget, GTK_WIDGET (self));

  for (row = 0; row < 3; row++)
    {
      for (col = 0; col < 4; col++)
        {
          GtkWidget *cell;

          text = g_strdup_printf ("%d %d", row, col);

          cell = gtk_label_new (text);

          gtk_widget_show (cell);

          gtk_grid_attach (GTK_GRID (widget), cell, col, row, 1, 1);
        }
    }
}

static void
my_widget_add_child (GtkBuildable *buildable,
                     GtkBuilder   *builder,
                     GObject      *child,
                     const gchar  *type)
{
  MyWidget *self = MY_WIDGET (buildable);

  if (type && strcmp (type, "header") == 0)
    setup_header_widget (self, GTK_WIDGET (child));
  else if (type && strcmp (type, "grid") == 0)
    setup_cell_grid (self, GTK_WIDGET (child));
  else
    GTK_BUILDER_WARN_INVALID_CHILD_TYPE (buildable, type);
}

static void
gtk_buildable_interface_init (GtkBuildableIface *iface)
{
  iface->add_child = my_widget_add_child;
}

static void
my_widget_class_init (MyWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gtk-builder/example/my-widget.ui");
}

static void
my_widget_init (MyWidget *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
