#include "my-app.h"
#include <gtk/gtk.h>

int
main (int argc, char** argv)
{
  MyApp *app;
  int status;

  app = my_app_new ();

  status = g_application_run (G_APPLICATION (app), argc, argv);

  return status;
}

